import React from 'react';

import storage from '../../../utils/storage';

// Components
import { Link } from 'react-router-dom/';

const Logo = () => (
  <Link to="/" onClick={() => storage.store('user', {})}>
    <img src="https://picsum.photos/64" alt="Logo" />
  </Link>
);

export default Logo;
