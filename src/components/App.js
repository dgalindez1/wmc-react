import React from 'react';
// npm i -s react-router-dom
import { BrowserRouter, Route } from 'react-router-dom';

// Components
import Login from './Login/Login';
import Header from './Header/Header';
import List from './List/List';

function App() {
  // BrowserRouter is a Higher Order Component
  return (
    <BrowserRouter>
      <Route component={Header} />
      <Route exact path="/" component={Login} />
      <Route path="/list" component={List} />
    </BrowserRouter>
  );
}

export default App;
